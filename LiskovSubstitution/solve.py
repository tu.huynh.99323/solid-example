class Bird:
    pass

class FlyingBirds(Bird):
    def fly(self):
        print("flying")

class Eagle(FlyingBirds):
    pass

class Penguin(Bird):
    pass

eagle = Eagle()
penguin = Penguin()

eagle.fly()
penguin.fly()