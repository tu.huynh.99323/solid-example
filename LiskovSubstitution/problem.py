class Bird:
    def fly(self):
        print("flying")

class Eagle(Bird):
    pass

class Penguin(Bird):
    pass

eagle = Eagle()
penguin = Penguin()

eagle.fly()
penguin.fly() # --> penguin can't fly !