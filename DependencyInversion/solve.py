class ILogginable:
    def doLogin(self):
        if (login_service):
            print("loggable")
        else:
            print("error login")

class IWriteLoggable:
    def writeLogFile(self):
        if (file_stream_service):
            print("writable")
        else:
            print("error write")

class LoginInterface:
    
    login = ILogginable()
    login.doLogin()

    write_log = IWriteLoggable()
    write_log.writeLogFile()
