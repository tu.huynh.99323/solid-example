class Dog:
    pass

class Chihuahua(Dog):
    pass

class Husky(Dog):
    pass

def barking(dog):
    if (isinstance(dog, Chihuahua)):
        print("gau gau")
    elif (isinstance(dog, Husky)):
        print("woof woof")
    else:
        pass

Bi = Chihuahua()
Ki = Husky()

barking(Bi)
barking(Ki)