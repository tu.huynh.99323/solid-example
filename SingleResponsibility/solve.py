class Animal:
    def doSomething(self):
        pass

class Dog(Animal):
    def doSomething(self):
        print("barking")

class Bird(Animal):
    def doSomething(self):
        print("flying")

class Fish(Animal):
    def doSomething(self):
        print("swimming")

Ki = Dog()
Ki.doSomething()