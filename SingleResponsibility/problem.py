class Animal:

    def bark(self):
        print("barking")

    def fly(self):
        print("flying")

    def swim(self):
        print("swimming")

Ki = Animal()
Ki.bark()
Ki.fly()
Ki.swim()