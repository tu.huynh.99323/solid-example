class AnimalInterface:
    def run(self):
        pass
    def swim(self):
        pass
    def fly(self):
        pass

class Dog(AnimalInterface):
    def run(self):
        print("run")
    def swim(self):
        print("swim") 
    def fly(self):
        # Dog can't fly so don't need to implement this
        pass

class Fish(AnimalInterface):
    def run(self):
        # Fish can't run
        pass
    def swim(self):
        print("swim")
    def fly(self):
        # Fish can't fly
        pass

# Inherit or implement the whole interface is redundant