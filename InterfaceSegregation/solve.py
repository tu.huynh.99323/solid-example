class AnimalInterface:
    def run(self):
        pass
    def swim(self):
        pass
    def fly(self):
        pass

class RunnableAnimalInterface:
    def run(self):
        print("run")

class SwimmableAnimalInterface:
    def swim(self):
        print("swim")

class FlyableAnimalInterface:
    def fly(self):
        print("fly")

class Dog(RunnableAnimalInterface, SwimmableAnimalInterface):
    pass

class Fish(SwimmableAnimalInterface):
    pass